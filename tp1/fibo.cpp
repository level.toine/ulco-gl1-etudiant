#!/bin/sh

c++ -o fibo.out fibo.cpp

rm -f fibo.csv

for i in 'seq 42'; do
    ./fibo.out $i | tail -n 1 >> fibo.csv
done

gnuplot -e "set out 'fibo_gnuplot.png';\
            set terminal png size 640, 360; \
            set style data linespoints; \
            set grid xtics ytics; \
            plot 'fibo.csv' using 1:3"
    
