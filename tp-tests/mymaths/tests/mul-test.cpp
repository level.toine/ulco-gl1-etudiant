#include "mymaths/mul.hpp"

#include <catch2/catch.hpp>

TEST_CASE("test mul2"){
    REQUIRE (mul2(40) == 80);
}

TEST_CASE("test muln"){
    REQUIRE (muln(5,4)==20);
}